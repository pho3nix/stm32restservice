using System;
using STM32Commons.Models;
using STM32Commons.Mapper;
using STM32Commons.Utils;
using STM32WebService.Utils;
using System.Threading;
using System.Globalization;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace STM32WebService.Handlers
{
    class MessageHandler
    {
        private SerialUtils serial;
        //private ArrayList received;

        public MessageHandler(SerialUtils serial)
        {
            this.serial = serial;
            //serial.addDataReceivedListener(onDataReceived);
        }
        
        //private void onDataReceived(string data)
        //{
        //    Console.WriteLine("Received: " + data);
        //    try {
        //        Message received = MessageMapper.deserialize(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Invalid message received. "+ex.Message);
        //    }
        //}

        public Message sendSync(Message request)
        {
            Message response = new Message();
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                string requestString = MessageMapper.serialize(request);
                Console.WriteLine("Request: " + requestString);
                string responseString = serial.syncWrite(requestString);
                Console.WriteLine("Response: " + responseString);
                response = MessageMapper.deserialize(responseString);
            }
            if (response.Opcode == Commands.ERROR)
            {
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.BadRequest);
                message.Content = new StringContent(response.getPayloadAsString());
                throw new HttpResponseException(message);
            }
            return response;
        }
        
    }
}