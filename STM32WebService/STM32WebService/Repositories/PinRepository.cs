﻿using STM32Commons.Models;
using STM32WebService.Handlers;
using System.Collections.Generic;
using STM32Commons.Utils;
using System.Linq;
using STM32WebService.Utils;
using System;
using System.Globalization;
using System.Threading;
using System.Collections;
using STM32WebService.Models;
using STM32Common.Utils;

namespace STM32WebService.Repositories
{
    public class PinRepository : IPinRepository
    {
        private MessageHandler handler;
        private long boardTicks = 0;
        private long localTicks = 0;

        public PinRepository():this("COM1"){}

        public PinRepository(string comPort)
        {
            SerialUtils serial = new SerialUtils(comPort, 9600);
            handler = new MessageHandler(serial);
            boardTicks = getDate();
            localTicks = DateTime.Now.Ticks;
        }

        public long getDate()
        {
            Message request = new Message();
            request.Opcode = Commands.GET_DATE;
            Message response = handler.sendSync(request);
            return long.Parse(response.getPayloadAsString());
        }

        public Pin getPin(string pinName)
        {
            Message request = new Message();
            request.Opcode = Commands.GET_PIN;
            request.setPayloadAsString(pinName);

            Message response = handler.sendSync(request);

            Pin result = (Pin)response.getPayloadAsObject();

            return result;
        }

        public List<Pin> getPins()
        {
            Message request = new Message();
            request.Opcode = Commands.GET_PINS;

            Message response = handler.sendSync(request);

            List<Pin> result = new List<Pin>();
            if (response.Payload != null)
            {
                result = (response.Payload).Cast<Pin>().ToList();
            }

            return result;
        }
        
        public Pin setPin(Pin pin)
        {
            Message request = new Message();
            request.Opcode = Commands.SET_PIN;
            request.setPayloadAsObject(pin);

            Message response = handler.sendSync(request);

            return (Pin)response.getPayloadAsObject();
        }

        public PinValue getValue(string pinName)
        {
            Message request = new Message();
            request.Opcode = Commands.READ_PIN;
            request.setPayloadAsString(pinName);

            Message response = handler.sendSync(request);

            PinValue result = (PinValue)response.getPayloadAsObject();
            
            return convertTimestamp(result);
        }

        public PinValue setValue(PinValue value)
        {
            Message request = new Message();
            request.Opcode = Commands.WRITE_PIN;
            request.setPayloadAsObject(value);

            Message response = handler.sendSync(request);

            return (PinValue)response.getPayloadAsObject();
        }

        public List<PinValue> getValues()
        {
            Message request = new Message();
            request.Opcode = Commands.READ_PINS;

            Message response = handler.sendSync(request);

            List<PinValue> result = new List<PinValue>();
            if (response.Payload != null)
            {
                result = (response.Payload).Cast<PinValue>().ToList();
                for (int i = 0; i < result.Count; i++)
                {
                    result[i] = convertTimestamp(result[i]);
                }
            }

            return result;
        }

        public List<PinValue> getHistory(string pinName)
        {
            Message request = new Message();
            request.Opcode = Commands.CONSUME_PIN_HIST;
            request.setPayloadAsString(pinName);

            Message response = handler.sendSync(request);

            List<PinValue> result = new List<PinValue>();
            if (response.Payload != null)
            {
                result = (response.Payload).Cast<PinValue>().ToList();
                for (int i=0 ; i<result.Count ; i++) {
                    result[i] = convertTimestamp(result[i]);
                }
            }

            return result;
        }

        private PinValue convertTimestamp(PinValue value)
        {
            long bTime = long.Parse(value.Timestamp);

            long diff = bTime - boardTicks;

            long lTime = localTicks + diff;

            value.Timestamp = new DateTime(lTime).ToString("yyyy/MM/dd HH:mm:ss.ffff");

            return value;
        }

        public string setController(Controller controller)
        {
            if(controller.OutputMax == 0 && controller.OutputMin == 0)
            {
                controller.OutputMax = 1;
                controller.OutputMin = 0;
            }
            Message request = new Message();
            request.Opcode = Commands.SET_CONTROLLER;

            PinValue setPoint = new PinValue();
            setPoint.Name = controller.InputPinName;
            setPoint.Value = controller.SetPoint.ToString();

            PinValue kp = new PinValue();
            kp.Name = controller.InputPinName;
            kp.Value = controller.Kp.ToString();

            PinValue outMax = new PinValue();
            outMax.Name = controller.OutputPinName;
            outMax.Value = controller.OutputMax.ToString();

            PinValue outMin = new PinValue();
            outMin.Name = controller.OutputPinName;
            outMin.Value = controller.OutputMin.ToString();

            request.setPayloadAsEnumerable(new ArrayList() { setPoint, kp, outMin,outMax});
            Message response = handler.sendSync(request);
            return response.getPayloadAsString();
        }

    }
}
