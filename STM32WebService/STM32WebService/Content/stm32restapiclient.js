﻿function initialize(comPort, callback) {
    var data = { "Port": comPort.toUpperCase() };
    return postJsonp("http://localhost:51123/api/pins/initialize", data, callback);
}

function getPin(pinName, callback) {
    return getJsonp("http://localhost:51123/api/pins/get/" + pinName.toUpperCase(), callback);
}

function getPins(callback) {
    return getJsonp("http://localhost:51123/api/pins/get", callback);
}

function setController(pinIn, pinOut, lux, kp, callback) {
    var rb = Math.pow(lux, -0.8)*151429.7627;
    var set_point_normalizzato = rb / (rb + 10000);
    var data = {
        "InputPinName": pinIn.toUpperCase(),
        "OutputPinName": pinOut.toUpperCase(),
        "SetPoint": set_point_normalizzato,
        "Kp": kp
    };
    return postJsonp("http://localhost:51123/api/pins/setcontroller", data, callback);
}

function setPin(pinName, mode, freq, callback) {
    var data = { "Name": pinName.toUpperCase(), "Mode": mode, "Frequency": freq };
    return postJsonp("http://localhost:51123/api/pins/set", data, callback);
}

function readValue(pinName, callback) {
    return getJsonp("http://localhost:51123/api/pins/read/" + pinName.toUpperCase(), callback);
}

function readValues(callback) {
    return getJsonp("http://localhost:51123/api/pins/read", callback);
}

function writeValue(pinName, value, callback) {
    var data = { "Value": value, "Name": pinName.toUpperCase() };
    return postJsonp("http://localhost:51123/api/pins/write", data, callback);
}

function readHistory(pinName, callback) {
    return getJsonp("http://localhost:51123/api/pins/history/" + pinName.toUpperCase(), callback);
}

function getJsonp(url, callback) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "json",
        timeout: 30000,
        success: function (response) {
            callback(response);
        },
        error: function (response) {
            console.log(JSON.stringify(response));
            //alert(response.statusText + ": " + response.responseText);
        }
    });
}

function postJsonp(url, data, callback) {
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        timeout: 30000,
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            callback(response);
        },
        error: function (response) {
            console.log(JSON.stringify(response));
        }
    });
}
