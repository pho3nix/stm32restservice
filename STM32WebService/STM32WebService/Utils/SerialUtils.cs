﻿using System;
using System.IO.Ports;

namespace STM32WebService.Utils
{
    public class SerialUtils
    {
        private SerialPort com;
        private bool running = false;
        private object blocker = new object();
        private object blocker_safe = new object();

        public delegate void OnDataReceived(string data);
        private OnDataReceived onDataReceived;
        
        public SerialUtils(string port, int speed)
        {
            com = new SerialPort(port, speed, Parity.None, 8, StopBits.One);
            start();
        }

        private void start()
        {
            // Begin communications
            com.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
            com.Open();
            running = true;
        }

        public string syncWrite(string request)
        {
            //TODO: Add timeout
            string response = null;
            lock (blocker)
            {
                lock (blocker_safe) {
                    com.Write(request + "\r\n");
                    response = com.ReadTo("\r\n");
                }
            }
            return response;
        }

        private string readed = "";
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Show all the incoming data in the port's buffer
            lock (blocker)
            {
                lock (blocker_safe)
                {
                    readed += com.ReadExisting();
                    if (readed!=null && readed.Length > 0) {
                        if (readed.Contains("\r\n"))
                        {
                            string[] lines = readed.Split("\r\n".ToCharArray());
                            foreach (string line in lines)
                            {
                                if (line!= null && line.Length > 0)
                                {
                                    onDataReceived(line);
                                }
                            }
                            readed = null;
                        }
                    }
                }
            }
        }

        public void addDataReceivedListener(OnDataReceived listener)
        {
            onDataReceived = listener;
            com.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }

        public void removeDataReceivedListener()
        {
            com.DataReceived -= new SerialDataReceivedEventHandler(port_DataReceived);
            onDataReceived = null;
        }

        private void stop()
        {
            if (com != null) {
                com.Close();
                running = false;
            }
        }

        ~SerialUtils()  // destructor
        {
            stop();
        }
    }
}