﻿using STM32Commons.Models;
using STM32WebService.Models;
using STM32WebService.Repositories;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace STM32WebService.Controllers
{
    public class PinsController : ApiController
    {
        private static IPinRepository repository;

        // GET: api/pins/get
        [HttpGet]
        public IEnumerable<Pin> Get()
        {
            return repository.getPins();
        }

        // GET: api/pins/get/PA1
        [HttpGet]
        public Pin Get(string id)
        {
            return repository.getPin(id);
        }

        // GET: api/pins/read
        [HttpGet]
        public IEnumerable<PinValue> Read()
        {
            return repository.getValues();
        }

        // GET: api/pins/read/PA1
        [HttpGet]
        public PinValue Read(string id)
        {
            return repository.getValue(id);
        }

        // GET: api/pins/history/PA1
        [HttpGet]
        public IEnumerable<PinValue> History(string id)
        {
            return repository.getHistory(id);
        }

        // POST: api/pins/set
        [HttpPost]
        public Pin Set(Pin pin)
        {
            return repository.setPin(pin);
        }

        // POST: api/pins/write
        [HttpPost]
        public PinValue Write(PinValue value)
        {
            return repository.setValue(value);
        }

        [HttpPost]
        public string Initialize(Com com)
        {
            try
            {
                repository = new PinRepository(com.Port);
                return "Repository inizializzato";
            }
            catch (Exception e)
            {
                return "Inizializzazione del repository fallita: " + e.Message;
            }

        }

        [HttpPost]
        public string SetController(Controller controller)
        {
            return repository.setController(controller);
        }

    }
}
