﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STM32WebService.Models
{
    public class Controller
    {
        public string InputPinName { get; set; }
        public string OutputPinName { get; set; }
        public double SetPoint { get; set; }
        public double Kp { get; set; }
        public double OutputMin { get; set; }
        public double OutputMax { get; set; }
    }
}