﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="../Content/assets/img/favicon.ico" type="image/x-icon">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../Content/assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Fonts  -->
    <link rel="stylesheet" href="../Content/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../Content/assets/css/simple-line-icons.css">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="../Content/assets/css/animate.css">
    <!-- Daterange Picker -->
    <link rel="stylesheet" href="../Content/assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- Calendar demo -->
    <link rel="stylesheet" href="../Content/assets/css/clndr.css">
    <!-- Switchery -->
    <link rel="stylesheet" href="../Content/assets/plugins/switchery/switchery.min.css">
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="../Content/assets/css/main.css">
    <!-- Feature detection -->
    <script src="../Content/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../Content/assets/js/vendor/html5shiv.js"></script>
    <script src="../Content/assets/js/vendor/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <section id="main-wrapper" class="theme-default sidebar-mini">
        <header id="header">
            <!--logo start-->
            <div class="brand">
                <a href="index.html" class="logo">
                    <i class="icon-layers"></i>
                    <span>STM32</span>DASHBOARD</a>
            </div>
            <!--logo end-->
            <ul class="nav navbar-nav navbar-left">
                <li class="toggle-navigation toggle-left">
                    <button class="sidebar-toggle" id="toggle-left">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
                <li class="toggle-profile hidden-xs">
                    <button type="button" class="btn btn-default" id="toggle-profile">
                        <i class="icon-user"></i>
                    </button>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                 <li class="toggle-fullscreen hidden-xs">
                    <button type="button" class="btn btn-default expand" id="toggle-fullscreen">
                        <i class="fa fa-expand"></i>
                    </button>
                </li>
                <li class="toggle-navigation toggle-right">
                    <button class="sidebar-toggle" id="toggle-right">
                        <i class="fa fa-indent"></i>
                    </button>
                </li>
            </ul>
        </header>
        <!--sidebar left start-->
        <aside class="sidebar sidebar-left">
            <nav>
                <h5 class="sidebar-header">Navigation</h5>
                <ul class="nav nav-pills nav-stacked">
                    <li class="active">
                        <a href="index.html" title="Dashboard">
                            <i class="fa  fa-fw fa-tachometer"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="UI Elements">
                            <i class="fa  fa-fw fa-cogs"></i> UI Elements
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="ui-buttons.html" title="Buttons">
                                     Buttons
                                </a>
                            </li>
                            <li>
                                <a href="ui-sliders-progress.html" title="Sliders &amp; Progress">
                                     Sliders &amp; Progress
                                </a>
                            </li>
                            <li>
                                <a href="ui-modals-popus.html" title="Modals &amp; Popups">
                                     Modals &amp; Popups
                                </a>
                            </li>
                            <li>
                                <a href="ui-tabs-accordions.html" title="Tabs &amp; Accordions">
                                     Tabs &amp; Accordions
                                </a>
                            </li>

                            <li>
                                <a href="ui-alerts-notifications.html" title="Alerts &amp; Notifications">
                                     Alerts &amp; Notifications
                                </a>
                            </li>
                            <li>
                                <a href="ui-nestable-lists.html" title=" Nestable Lists">
                                     Nestable Lists
                                </a>
                            </li>
                            <li>
                                <a href="ui-panels.html" title="Panels">
                                     Panels
                                </a>
                            </li>
                            <li>
                                <a href="ui-icons.html" title="Icons">
                                     Icons
                                </a>
                            </li>
                            <li>
                                <a href="ui-typography.html" title="Typography">
                                     Typography
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Forms">
                            <i class="fa  fa-fw fa-edit"></i> Forms
                        </a>
                        <ul class="nav-sub">
                            <li><a href="forms-components.html" title="Components">Components</a>
                            </li>
                            <li><a href="forms-validation.html" title="Validation">Validation</a>
                            </li>
                            <li><a href="forms-mask.html" title="Mask">Mask</a>
                            </li>
                            <li><a href="forms-wizard.html" title="Wizard">Wizard</a>
                            </li>
                            <li><a href="forms-multiple-file.html" title="Multiple File Upload">Multiple File Upload</a>
                            </li>
                            <li><a href="forms-wysiwyg.html" title="WYSIWYG Editor">WYSIWYG Editor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Tables">
                            <i class="fa  fa-fw fa-th-list"></i> Tables
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="tables-basic-tables.html" title="Basic Tables">
                                     Basic Tables
                                </a>
                            </li>
                            <li>
                                <a href="tables-data-tables.html" title="Data Tables">
                                     Data Tables
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Charts">
                            <i class="fa fa-fw fa-bar-chart-o"></i> Charts
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="charts-chartjs.html" title="Chartjs">
                                    Chartjs
                                </a>
                            </li>
                            <li>
                                <a href="charts-c3.html" title="C3 Charts">
                                     C3 Charts
                                </a>
                            </li>
                            <li>
                                <a href="charts-morris.html" title="Morris.js Charts">
                                     Morris.js Charts
                                </a>
                            </li>
                            <li>
                                <a href="charts-sparkline.html" title="Sparkline Charts">
                                     Sparkline Charts
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                         <a href="#" title="Mail">
                            <i class="fa fa-fw fa-envelope-o"></i> Mail
                            <span class="label label-primary label-circle pull-right">8</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="mail-inbox.html" title="Mail Inbox">
                                    Inbox
                                </a>
                            </li>
                            <li>
                                <a href="mail-compose.html" title="Mail Compose">
                                     Compose Mail
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Maps">
                            <i class="fa  fa-fw fa-map-marker"></i> Maps
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="maps-google.html" title="Google Maps">
                                     Google Maps
                                </a>
                            </li>
                            <li>
                                <a href="maps-vector.html" title="Vector Maps">
                                     Vector Maps
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../Frontend/" title="Frontend">
                            <i class="fa  fa-fw fa-desktop"></i> Front-end Theme
                            <span class="pull-right badge badge-danger">new</span>
                        </a>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Pages">
                            <i class="fa  fa-fw fa-file-text"></i> Pages
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="pages-blank-page.html" title="Blank Page">
                                     Blank Page
                                </a>
                            </li>

                            <li>
                                <a href="pages-profile.html" title="Profile">
                                     Profile
                                </a>
                            </li>
                            <li>
                                <a href="pages-sign-in.html" title="Sign In">
                                     Sign In
                                </a>
                            </li>
                            <li>
                                <a href="pages-sign-up.html" title="Sign Up">
                                     Sign Up
                                </a>
                            </li>
                            <li>
                                <a href="pages-locked-screen.html" title="Locked Screen">
                                     Locked Screen
                                </a>
                            </li>
                            <li>
                                <a href="pages-404.html" title="404 Page">
                                     404 Page
                                </a>
                            </li>
                            <li>
                                <a href="pages-500.html" title="500 Page">
                                     500 Page
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-dropdown">
                        <a href="#" title="Menu Levels">
                            <i class="fa  fa-fw fa-folder-open"></i> Menu Levels
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="javascript:;" title="Level 2.1">
                                    <i class="fa fa-fw fa-file"></i> Level 1.1
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" title="Level 2.2">
                                    <i class="fa fa-fw fa-file"></i> Level 1.2
                                </a>
                            </li>
                            <li class="nav-dropdown">
                                <a href="#" title="Level 2.3">
                                    <i class="fa fa-fw fa-folder-open"></i> Level 1.3
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="javascript:;" title="Level 3.1">
                                            <i class="fa fa-fw fa-file"></i> Level 2.1
                                        </a>
                                    </li>
                                    <li class="nav-dropdown">
                                        <a href="#" title="Level 3.2">
                                            <i class="fa fa-fw fa-folder-open"></i> Level 2.2
                                        </a>
                                        <ul class="nav-sub">
                                            <li>
                                                <a href="javascript:;" title="Level 4.1">
                                                    <i class="fa fa-fw fa-file"></i> Level 3.1
                                                </a>
                                            </li>
                                            <li class="nav-dropdown">
                                                <a href="#" title="Level 4.2">
                                                    <i class="fa fa-fw fa-folder-open"></i> Level 3.2
                                                </a>
                                                <ul class="nav-sub">
                                                    <li class="nav-dropdown">
                                                        <a href="#" title="Level 5.1">
                                                            <i class="fa fa-fw fa-folder-open"></i> Level 4.1
                                                        </a>
                                                        <ul class="nav-sub">
                                                            <li>
                                                                <a href="javascript:;" title="Level 6.1">
                                                                    <i class="fa fa-fw fa-file"></i> Level 5.1
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" title="Level 6.2">
                                                                    <i class="fa fa-fw fa-file"></i> Level 5.2
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" title="Level 5.2">
                                                            <i class="fa fa-fw fa-file"></i> Level 4.2
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" title="Level 5.3">
                                                            <i class="fa fa-fw fa-file"></i> Level 4.3
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="animations.html" title="CSS Animations">
                            <i class="fa  fa-fw fa-magic"></i> CSS Animations
                        </a>
                    </li>
                </ul>
            </nav>
            <h5 class="sidebar-header">Account Settings</h5>
            <div class="setting-list">
                <div class="row">
                    <div class="col-xs-8">
                        <label for="check1" class="control-label">Share your status</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="checkbox" class="js-switch" checked id="check1" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <label for="check2" class="control-label">Push Notifications</label>
                    </div>
                    <div class="col-xs-4">
                        <input type="checkbox" class="js-switch" id="check2" />
                    </div>
                </div>
            </div>
        </aside>
        <!--sidebar left end-->
        <!--main content start-->
        <section class="main-content-wrapper">
            <div class="pageheader">
                <h1>Dashboard</h1>
                <div class="breadcrumb-wrapper hidden-xs">
                    <span class="label">You are here:</span>
                    <ol class="breadcrumb">
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <section id="main-content" class="animated fadeInUp">

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Controller parameters</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                    <i class="fa fa-chevron-down"></i>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form role="form">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                                <label for="analog_input_port">Analog input port</label>
                                                <select id="input_port" class="form-control input-lg">
                                                    <option value="PA0">PA0</option>
                                                    <option value="PA1">PA1</option>
                                                    <option value="PA2">PA2</option>
                                                    <option value="PA3">PA3</option>
                                                </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Frequenza campionamento</label>
                                            <input id="freq_input" type="text" class="form-control" value="5">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                                <label for="pwm_output_port">PWM output port</label>
                                                <select id="output_port" class="form-control input-lg">
                                                    <option value="PD12">PD12</option>
                                                    <option value="PD13">PD13</option>
                                                    <option value="PD14">PD14</option>
                                                    <option value="PD15">PD15</option>
                                                </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Frequenza PWM</label>
                                            <input id="freq_output" type="text" class="form-control" value="100">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Set Point (Lux)</label>
                                            <input id="set_point" type="text" class="form-control" value="500">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label">Kp</label>
                                            <input id="kp" type="text" class="form-control" value="-1">
                                        </div>
                                    </div>
                                </form>
                                <div class="pull-right">
                                    <button onclick="setPController($('#input_port').val(), $('#freq_input').val(), $('#output_port').val(), $('#freq_output').val(), $('#set_point').val(), $('#kp').val())" class="btn btn-primary">Set</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Measured luminosity</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                        <span class="text-left">ADC output</span>
                                        <div class="progress">
                                            <div id="adc_out" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                                        <span class="text-left">Volt output</span>
                                        <div class="progress">
                                            <div id="volt_out" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                                        <span class="text-left">Lux output</span>
                                        <div class="progress">
                                            <div id="lux_out" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                                        <span class="text-left">Error</span>
                                        <div class="progress">
                                            <div id="error_out" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Polling controls</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Update interval</label>
                                    <input id="update_interval" type="text" class="form-control" value="1000">
                                </div>
                                <div class="pull-right">
                                    <button onclick="stopPolling()" class="btn btn-danger">Stop</button>
                                    <button onclick="startPolling()" class="btn btn-primary">Start</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Percental luminosity</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                </div>
                            </div>
                            <div class="panel-body widget-gauge">
                                <canvas width="160" height="100" id="gauge" class=""></canvas>
                                <div class="goal-wrapper">
                                    <span id="gauge-text" class="gauge-value pull-left">0</span>
                                    <span class="gauge-value pull-left">%</span>
                                    <span id="goal-text" class="goal-value pull-right">100%</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                   <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Data Tables</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                    <i class="fa fa-chevron-down"></i>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <div class="panel-body">
                                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Timestamp</th>
                                            <th>ADC Value</th>
                                            <th>Lux Value</th>
                                            <th>Lux Error</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">History (last 15 points)</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-expand"></i>
                                    <i class="fa fa-chevron-down"></i>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <div class="panel-body text-center">
                                <div>
                                    <canvas id="line" height="140"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </section>
        <!--main content end-->
    </section>
    <!--sidebar right start-->
    <aside id="sidebar-right">
        <h4 class="sidebar-title">contact List</h4>
        <div id="contact-list-wrapper">
            <div class="heading">
                <ul>
                    <li class="new-contact"><a href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                    </li>
                    <li>
                        <input type="text" class="search" placeholder="Search">
                        <button type="submit" class="btn btn-sm btn-search"><i class="fa fa-search"></i>
                        </button>
                    </li>
                </ul>
            </div>
            <div id="contact-list">
                <ul>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar3.png" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Ashley Bell </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Sarasota, FL</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar1.png" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Brian Johnson </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> San Francisco, CA</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar2.png" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Chris Jones </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Brooklyn, NY</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar4.jpg" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Erica Hill </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Palo Alto, Ca</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar5.png" class="img-circle" alt="">
                          <i class="away animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Greg Smith </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> London, UK</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar6.png" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Jason Kendall</div>
                                <small class="location text-muted"><i class="icon-pointer"></i> New York, NY </small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar7.png" class="img-circle" alt="">
                          <i class="on animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Kristen Davis </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Greenville, SC</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar8.png" class="img-circle off" alt="">
                          <i class="off animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Michael Shepard </div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Vancouver, BC</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="avatar">
                        <img src="../Content/assets/img/avatar9.png" class="img-circle off" alt="">
                          <i class="off animated bounceIn"></i>
                        </span>
                            </div>
                            <div class="col-md-9">
                                <div class="name">Paul Allen</div>
                                <small class="location text-muted"><i class="icon-pointer"></i> Savannah, GA</small>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="contact-user">
                <div class="chat-user active"><span><i class="icon-bubble"></i></span>
                </div>
                <div class="email-user"><span><i class="icon-envelope-open"></i></span>
                </div>
                <div class="call-user"><span><i class="icon-call-out"></i></span>
                </div>
            </div>
        </div>
    </aside>
    <!--/sidebar right end-->
    <!--Global JS-->
    <script src="../Content/jquery-3.2.1.min.js"></script>
    <script src="../Content/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../Content/assets/plugins/navgoco/jquery.navgoco.min.js"></script>
    <script src="../Content/assets/plugins/pace/pace.min.js"></script>
    <script src="../Content/assets/plugins/fullscreen/jquery.fullscreen-min.js"></script>
    <script src="../Content/assets/js/src/app.js"></script>
    <!--Page Level JS-->
    <script src="../Content/assets/plugins/countTo/jquery.countTo.js"></script>
    <script src="../Content/assets/plugins/weather/js/skycons.js"></script>
    <script src="../Content/assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="../Content/assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- ChartJS  -->
    <script src="../Content/assets/plugins/chartjs/Chart.min.js"></script>
    <script src="../Content/assets/plugins/chartjs/chartjs-demo.js"></script>
    <!-- Morris  -->
    <script src="../Content/assets/plugins/morris/js/morris.min.js"></script>
    <script src="../Content/assets/plugins/morris/js/raphael.2.1.0.min.js"></script>
    <!-- Vector Map  -->
    <script src="../Content/assets/plugins/jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../Content/assets/plugins/jvectormap/js/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Gauge  -->
    <script src="../Content/assets/plugins/gauge/gauge.min.js"></script>
    <script src="../Content/assets/plugins/gauge/gauge-demo.js"></script>
    <!-- Calendar  -->
    <script src="../Content/assets/plugins/calendar/clndr.js"></script>
    <script src="../Content/assets/plugins/calendar/clndr-demo.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <!-- Switch -->
    <script src="../Content/assets/plugins/switchery/switchery.min.js"></script>
    <!--Data table -->
    <script src="../Content/assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="../Content/assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <!--Load these page level functions-->

    <script src="../Content/stm32restapiclient.js"></script>
    <script>
        var table;
        var pin = null;
        var timer = null;
        var set_point = 0;
        $(document).ready(function () {
            table = $('#example').DataTable({
                "order": [[0, "desc"]],
                "pageLength": 55,
                "searching": false,
                "oLanguage": { "sLengthMenu": "\_MENU_" },
                "bLengthChange": false
            });
        });
        function setPController(pinIn, freqIn, pinOut, freqOut, lux, kp) {
            pin = pinIn;
            set_point = parseFloat(lux);
            setPin(pinOut, 4, freqOut, function (d) {});
            setPin(pinIn, 3, freqIn, function (d) {});
            setController(pinIn, pinOut, lux, kp, function (d) { });
            stopPolling();
            startPolling();
        }
        function stopPolling() {
            clearTimeout(timer);
        }
        function startPolling() {
            pollServer();
        }
        function pollServer() {
            timer = window.setTimeout(function () {
                readHistory(pin, pollResult);
            }, $("#update_interval").val());
        }
        function pollResult(data) {
            if (data !== undefined && data !== null && data.length > 0) {
                setTools(parseFloat(data[data.length-1].Value), data[data.length-1].Timestamp);
            }
            pollServer();
        }
        function setTools(value, timestamp) {
            var time = timestamp.substring(11, 21);
            var perc = value * 100;
            
            var adc_out = Math.round(value * 10000) / 10000;
            $("#adc_out").width(perc + "%");
            $("#adc_out").html(adc_out);
            
            $("#volt_out").width(perc+"%");
            $("#volt_out").html(Math.round(value * 3000) / 1000 + " V");

            var rb = (value*10000)/(1-value);
            var lux = Math.pow(rb, -1.25) * 2987198.29;
            //MAX 5000 lux
            var lux_normalized_perc = (lux * 100 / 2500);
            var lux_out = Math.round(lux * 100) / 100;
            $("#lux_out").width(lux_normalized_perc + "%");
            $("#lux_out").html(lux_out + " lx");

            var error = set_point - lux
            var error_out = Math.round(error * 100) / 100;
            if (error < 0) error = 0;
            $("#error_out").width(error*100/2500 + "%");
            $("#error_out").html(error_out + " lx");

            gauge.set(lux * 100 / 1000);
            updateChart(lux, time);
            updateTable(timestamp, adc_out, lux_out, error_out);
        }
        function updateTable(timestamp, value, lux, error) {
            //if (table.column(0).data().length == 15)
            //    table.row(':eq(14)').remove();
            table.row.add([timestamp, value, lux, error]).draw(true);
        }
        function updateChart(value, time) {
            //lineChartData.labels.push(time);
            //lineChartData.datasets[0].data.push(value);
            window.myLine.addData([value], time);
            window.myLine.removeData();
        }
    </script>
</body>

</html>
