﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>title</title>
</head>
<body>
    <div>
            <div style="margin:10px">
                COM: <input list="com_ports" id="com_port"/>
                <datalist id="com_ports">
                  <option value="COM1"/>
                  <option value="COM2"/>
                  <option value="COM3"/>
                  <option value="COM4"/>
                  <option value="COM5"/>
                  <option value="COM6"/>
                  <option value="COM7"/>
                  <option value="COM8"/>
                </datalist>
                <button onclick="initialize($('#com_port').val(),getResultInitialize)">
                INITIALIZE
                </button>
                <div id="result_initialize"></div>
            </div>
            <div style="margin:10px">DIGITAL OUTPUT = 1; DIGITAL INPUT = 2; ANALOG INPUT = 3; PWM = 4;</div>
            <div style="margin:10px">PIN: <input id="pin_name"/></div>
            <div style="margin:10px">
                <button onclick="getPin($('#pin_name').val(),getResultGetPin)">
                GET PIN
                </button>
                <button onclick="getPins(getResultGetPin)">
                GET PINS
                </button>
                <div id="result_get_pin"></div>
            </div>
            
            <div style="margin:10px">
                MODE: <input id="pin_mode"/>
                FREQ: <input id="pin_freq"/>
                <button onclick="setPin($('#pin_name').val(), $('#pin_mode').val(), $('#pin_freq').val(),getResultSetPin)">
                SET PIN
                </button>
                <div id="result_set_pin"></div>
            </div>

            <div style="margin:10px">
                <button onclick="readValue($('#pin_name').val(),getResultReadPin)">
                READ PIN
                </button>
                <div id="result_read_pin"></div>
            </div>

            <div style="margin:10px">
                VALUE: <input id="pin_value"/>
                <button onclick="writeValue($('#pin_name').val(), $('#pin_value').val(),getResultWritePin)">
                WRITE PIN
                </button>
                <div id="result_write_pin"></div>
            </div>
            <div style="margin:10px">
                <button onclick="startPolling($('#pin_name').val())">
                START READ PIN HISTORY
                </button>
                <button onclick="stopPolling()">
                STOP READ PIN HISTORY
                </button>
                <div id="result"></div>
            </div>
            <div style="margin:10px">
                PIN IN: <input id="pin_in"/>
                PIN OUT: <input id="pin_out"/>
                SET POINT: <input id="set_point"/>
                KP: <input id="kp"/>
                MIN OUT: <input id="min_out"/>
                MAX OUT: <input id="max_out"/>
                <button onclick="setController($('#pin_in').val(), $('#pin_out').val(), $('#set_point').val(), $('#kp').val(), $('#min_out').val(), $('#max_out').val(), getResultSetController)">
                SET CONTROLLER
                </button>
                <div id="result_set_controller"></div>
            </div>
    </div>
    <script src="../Content/jquery-3.2.1.min.js"></script>
    <script src="../Content/stm32restapiclient.js"></script>
    <script>
        var pin = null;
        var timer = null;
        $(document).ready(function () {

        });
        function stopPolling() {
            clearTimeout(timer);
        }
        function startPolling(pinName) {
            pin = pinName;
            pollServer();
        }
        function pollServer() {
                timer = window.setTimeout(function () {
                    readHistory(pin, pollResult);
                }, 1000);
        }
        function pollResult(data) {
            getResult(data);
            pollServer();
        }
        function getResultInitialize(data) {
            $('#result_initialize').text(JSON.stringify(data));
        }
        function getResult(data) {
            $('#result').text(JSON.stringify(data));
        }
        function getResultGetPin(data) {
            $('#result_get_pin').text(JSON.stringify(data));
        }
        function getResultReadPin(data) {
            $('#result_read_pin').text(JSON.stringify(data));
        }
        function getResultSetPin(data) {
            $('#result_set_pin').text(JSON.stringify(data));
        }
        function getResultSetController(data) {
            $('#result_set_controller').text(JSON.stringify(data));
        }
        function getResultWritePin(data) {
            $('#result_write_pin').text(JSON.stringify(data));
        }
    </script>
</body>
</html>
