/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Models\BoardPin.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    31-Mar-2017
  ******************************************************************************
  */

using System;
using Microsoft.SPOT.Hardware;
using STM32F407G_Discovery.Netmf.Hardware;
using System.Collections;
using STM32Commons.Models;
using STM32Commons.Utils;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class BoardPin : Pin
    {
        private object port;
        private int historySize = 25;
        private Queue history = new Queue();
        private object blocker = new object();

        public BoardPin(string pinName) : base(pinName)
        {
            ArrayList supportedModes = new ArrayList();
            if (GPIO.contains(pinName)) {
                supportedModes.Add(Modes.GPIO_INPUT);
                supportedModes.Add(Modes.GPIO_OUTPUT);
            }
            if (LED.contains(pinName) && !supportedModes.Contains(Modes.LED))
            {
                supportedModes.Add(Modes.LED);
            }
            if (PWMS.contains(pinName))
            {
                supportedModes.Add(Modes.PWM);
            }
            if (ADC.contains(pinName))
            {
                supportedModes.Add(Modes.ANALOG_INPUT);
            }
            if (BUTTON.contains(pinName) && !supportedModes.Contains(Modes.BUTTON))
            {
                supportedModes.Add(Modes.BUTTON);
            }
            this.SupportedModes = supportedModes.ToArray(typeof(int)) as int[];
        }

        protected override void disable()
        {
            if (port != null)
            {
                if (Mode == Modes.GPIO_INPUT)
                {
                    InputPort ip = (InputPort)port;
                    ip.Dispose();
                }
                else if (Mode == Modes.GPIO_OUTPUT)
                {
                    OutputPort op = (OutputPort)port;
                    op.Dispose();
                }
                else if (Mode == Modes.ANALOG_INPUT)
                {
                    AnalogInput ai = (AnalogInput)port;
                    ai.Dispose();
                }
                else if (Mode == Modes.PWM)
                {
                    PWM pwm = (PWM)port;
                    pwm.Dispose();
                }
                port = null;
            }
            history.Clear();
            Enabled = false;
        }

        protected override void enable()
        {
            if (port == null)
            {
                if (Mode == Modes.GPIO_INPUT)
                {
                    port = new InputPort(GPIO.get(Name), false, Port.ResistorMode.PullUp);
                }
                else if (Mode == Modes.GPIO_OUTPUT)
                {
                    port = new OutputPort(GPIO.get(Name), false);
                }
                else if (Mode == Modes.ANALOG_INPUT)
                {
                    port = new AnalogInput(ADC.get(Name));
                }
                else if (Mode == Modes.PWM)
                {
                    port = new PWM(PWMS.get(Name), 0.1, 0.1, false);
                }
                else
                {
                    throw new Exception("Unknown pin mode");
                }
            }
            Enabled = true;
        }
        
        private void setPWM(double freq, double dutycycle)
        {
            if (Mode != Modes.PWM) throw new Exception("Unsupported method for current pin mode");
            ((PWM)port).Frequency = freq;
            ((PWM)port).DutyCycle = dutycycle;
            ((PWM)port).Start();
        }

        private void setDigital(bool state)
        {
            if (Mode != Modes.GPIO_OUTPUT) throw new Exception("Unsupported method for current pin mode");
            ((OutputPort)port).Write(state);
        }

        private bool getDigital()
        {
            if (Mode != Modes.GPIO_INPUT) throw new Exception("Unsupported method for current pin mode");
            return ((InputPort)port).Read();
        }

        private double getAnalog()
        {
            if (Mode != Modes.ANALOG_INPUT) throw new Exception("Unsupported method for current pin mode");
            return ((AnalogInput)port).Read();
        }

        public Queue History()
        {
            return history;
        }

        public Queue ConsumeHistory()
        {
            Queue result = new Queue();
            if (history.Count > 0) {
                lock (blocker)
                {
                    result = (Queue)history.Clone();
                    history.Clear();
                }
            }
            return result;
        }
        
        public PinValue Historicise()
        {
            PinValue value = null;
            if (CanRead()) {
                lock (blocker) {
                    if (history.Count > historySize)
                    {
                        history.Dequeue();
                    }
                    value = Read();
                    history.Enqueue(value);
                }
            }
            return value;
        }

        public bool CanRead()
        {
            return Mode == Modes.GPIO_INPUT || Mode == Modes.ANALOG_INPUT;
        }
      
        public PinValue Read()
        {
            PinValue value = new PinValue();
            value.Name = this.Name;
            value.Timestamp = DateTime.Now.Ticks.ToString();
            if (Mode == Modes.GPIO_INPUT)
            {
                value.Value = getDigital() ? "1" : "0";
            }
            else if (Mode == Modes.ANALOG_INPUT)
            {
                value.Value = getAnalog().ToString();
            }
            else
            {
                throw new Exception("Unsupported method for current pin mode");
            }
            return value;
        }

        public void Write(double value)
        {
            if (Mode == Modes.GPIO_OUTPUT)
            {
                setDigital(value>0);
                return;
            }
            if (Mode == Modes.PWM)
            {
                if (Frequency > 0)
                {
                    setPWM(Frequency, value);
                    return;
                }
                else
                {
                    throw new Exception("PWM frequency must be positive");
                }
            }
            throw new Exception("Unsupported method for current pin mode");
        }
        
    }
}
