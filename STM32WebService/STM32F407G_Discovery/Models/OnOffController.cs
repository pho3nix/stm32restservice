﻿using STM32Commons.Utils;
using System;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class OnOffController : Controller
    {
        public OnOffController(BoardPin input, BoardPin output, double inMin, double inMax, double outMin, double outMax) 
            : base(input, output, inMin, inMax, outMin, outMax)
        {
        }

        public override double Control(double readed)
        {
            if (readed >= par1B) OutputValue = par2B;
            else if (readed <= par1A) OutputValue = par2A;
            return OutputValue;
        }

    }
}