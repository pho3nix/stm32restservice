/**
  ******************************************************************************
  * @file    STM32F407G_Discovery.Netmf.Models\Board.cs
  * @author  Salvatore Alonge
  * @version V1.0.0
  * @date    31-Mar-2017
  ******************************************************************************
  */
using STM32F407G_Discovery.Netmf.Hardware;
using System.Collections;
using System;
using System.Threading;
using STM32Commons.Models;
using System.Text;
using STM32Common.Utils;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class Board
    {
        public UART uart = new UART(UART.COM1, 9600);
        private ArrayList pinout = new ArrayList();
        private Timer sampler = null;

        private int microtick = 0;
        private int tick = 100;
        private int macrotick = 60000;
        private Controller controller = null;

        public Board()
        {
            pinout.Add(new BoardPin("PA0"));
            pinout.Add(new BoardPin("PA1"));
            pinout.Add(new BoardPin("PA2"));
            pinout.Add(new BoardPin("PA3"));

            pinout.Add(new BoardPin("PA4"));
            pinout.Add(new BoardPin("PA5"));
            pinout.Add(new BoardPin("PA6"));
            pinout.Add(new BoardPin("PA7"));

            pinout.Add(new BoardPin("PA8"));
            pinout.Add(new BoardPin("PA9"));
            pinout.Add(new BoardPin("PA10"));
            pinout.Add(new BoardPin("PA11"));

            pinout.Add(new BoardPin("PA12"));
            pinout.Add(new BoardPin("PA13"));
            pinout.Add(new BoardPin("PA14"));
            pinout.Add(new BoardPin("PA15"));

            pinout.Add(new BoardPin("PC0"));
            pinout.Add(new BoardPin("PC1"));
            pinout.Add(new BoardPin("PC2"));
            pinout.Add(new BoardPin("PC3"));

            pinout.Add(new BoardPin("PC4"));
            pinout.Add(new BoardPin("PC5"));
            pinout.Add(new BoardPin("PC6"));
            pinout.Add(new BoardPin("PC7"));
            
            pinout.Add(new BoardPin("PD12"));
            pinout.Add(new BoardPin("PD13"));
            pinout.Add(new BoardPin("PD14"));
            pinout.Add(new BoardPin("PD15"));
        }

        public void setOnOffController(PinValue input1, PinValue input2, PinValue output1, PinValue output2)
        {
            this.setController(false, input1, input2, output1, output2);
        }

        public void setProportionalController(PinValue input1, PinValue input2, PinValue output1, PinValue output2)
        {
            this.setController(true, input1, input2, output1, output2);
        }

        private void setController(bool proportional, PinValue input1, PinValue input2, PinValue output1, PinValue output2)
        {
            BoardPin input = getPin(input1.Name);
            BoardPin output = getPin(output1.Name);
            if (!input.Enabled || !input.CanRead()) throw new Exception("The input pin is not configured correctly");
            if (!output.Enabled || output.CanRead()) throw new Exception("The output pin is not configured correctly");

            this.controller = new PController(input, output, 
                Conversion.tryParse(input1.Value), Conversion.tryParse(input2.Value),
                Conversion.tryParse(output1.Value), Conversion.tryParse(output2.Value));
        }

        public BoardPin getPin(string pinName)
        {
            foreach (BoardPin pin in pinout)
            {
                if (pin.Name.Equals(pinName))
                {
                    return pin;
                }
            }
            throw new Exception("Unknown pin");
        }

        public ArrayList getPinout()
        {
            return pinout;
        }

        public void startSampler()
        {
            startSampler(microtick, tick, macrotick);
        }

        public void startSampler(int microtick, int macrotick, int tick)
        {
            this.microtick = microtick;
            this.tick = tick;
            this.macrotick = macrotick;
            if (sampler != null) stopSampler();
            sampler = new Timer(new TimerCallback(pickSamples), "", this.tick, this.tick);
        }

        public void stopSampler()
        {
            if (sampler!=null)
            {
                sampler.Change(Timeout.Infinite, Timeout.Infinite);
                sampler.Dispose();
                sampler = null;
            }
        }

        private void pickSamples(object data)
        {
            microtick = microtick % macrotick;
            microtick ++;
            foreach (BoardPin pin in getPinout())
            {
                if (pin.CanRead()) {
                    int freq = pin.Frequency;
                    if (freq <= 0) continue;
                    if (microtick % freq == 0)
                    {
                        PinValue value = pin.Historicise();
                        if (controller != null) {
                            if (pin.Name.Equals(controller.InputPin))
                            {
                                BoardPin output = getPin(controller.OutputPin);
                                output.Write(controller.Control(Conversion.tryParse(value.Value)));
                            }
                        }
                    }
                }
            }
        }

    }
}
