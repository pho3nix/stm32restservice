﻿using STM32Commons.Utils;
using System;

namespace STM32F407G_Discovery.Netmf.Models
{
    public class PController : Controller
    {
        public PController(BoardPin input, BoardPin output, double inMin, double inMax, double outMin, double outMax) 
            : base(input, output, inMin, inMax, outMin, outMax)
        {
        }
        public override double Control(double readed)
        {
            var reference = par1A;//(InputMax + InputMin) / 2;
            var error = reference - readed;
            var output = error*par1B;
            if (output > par2B) OutputValue = par2B;
            else if (output < par2A) OutputValue = par2A;
            else OutputValue = output;
            return OutputValue;
        }

    }
}