﻿using STM32Commons.Utils;
using System;

namespace STM32F407G_Discovery.Netmf.Models
{
    public abstract class Controller
    {
        public Controller(BoardPin input, BoardPin output, double a, double b, double c, double d) {
            if (!input.CanRead()) throw new Exception("Inconsistent input pin mode");
            if (output.CanRead()) throw new Exception("Inconsistent output pin mode");
            InputPin = input.Name;
            OutputPin = output.Name;
            par1A = a;
            par1B = b;
            par2A = c;
            par2B = d;

        }

        public string InputPin { get; protected set; }
        public double par1A { get; protected set; }
        public double par1B { get; protected set; }

        public string OutputPin { get; protected set; }
        public double par2A { get; protected set; }
        public double par2B { get; protected set; }

        public double OutputValue { get; protected set; }

        public abstract double Control(double readed);
    }
}