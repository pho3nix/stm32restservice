using System;
using STM32F407G_Discovery.Netmf.Models;
using STM32Commons.Models;
using STM32Commons.Utils;
using System.Collections;
using Microsoft.SPOT.Hardware;
using STM32Common.Utils;

namespace STM32F407G_Discovery.Netmf.Handlers
{
    class MessageHandler
    {
        private Board board;

        public MessageHandler(Board board)
        {
            this.board = board;
        }

        public Message handleMessage(Message request)
        {
            Message response = new Message();
            if (request == null || request.Opcode == null)
            {
                throw new Exception("Empty or invalid Request");
            }
            else
            {
                switch (request.Opcode)
                {
                    case Commands.GET_DATE:
                        {
                            DateTime time = DateTime.Now;
                            response.Opcode = Commands.GET_DATE;
                            response.setPayloadAsString(time.Ticks.ToString());
                            break;
                        }
                    case Commands.GET_PINS:
                        {
                            response.Opcode = Commands.GET_PINS;
                            response.Payload = board.getPinout();
                            break;
                        }
                    case Commands.GET_PIN:
                        {
                            response.Opcode = Commands.GET_PIN;
                            string pinName = request.getPayloadAsString();
                            BoardPin pin = board.getPin(pinName);
                            response.setPayloadAsObject(pin);
                            break;
                        }
                    case Commands.SET_PIN:
                        {
                            response.Opcode = Commands.SET_PIN;
                            Pin pin = request.getPayloadAsObject() as Pin;
                            BoardPin bpin = board.getPin(pin.Name);
                            bpin.Mode = pin.Mode;
                            bpin.Frequency = pin.Frequency;
                            response.setPayloadAsObject(bpin);
                            break;
                        }
                    case Commands.READ_PIN:
                        {
                            response.Opcode = Commands.READ_PIN;
                            string pinName = request.getPayloadAsString();
                            response.setPayloadAsObject(board.getPin(pinName).Read());
                            break;
                        }
                    case Commands.READ_PINS:
                        {
                            response.Opcode = Commands.READ_PINS;
                            ArrayList values = new ArrayList();
                            foreach (BoardPin pin in board.getPinout())
                            {
                                if (pin.CanRead()) {
                                    try
                                    {
                                        PinValue value = pin.Read();
                                        values.Add(value);
                                    }
                                    catch (Exception ex)
                                    {
                                        //Cannot read this pin
                                    }
                                }
                            }
                            response.Payload = values;
                            break;
                        }
                    case Commands.WRITE_PIN:
                        {
                            response.Opcode = Commands.WRITE_PIN;
                            PinValue value = (PinValue) request.getPayloadAsObject();
                            board.getPin(value.Name).Write(Conversion.tryParse(value.Value));
                            response.setPayloadAsObject(value);
                            break;
                        }
                    case Commands.GET_PIN_HIST:
                        {
                            response.Opcode = Commands.GET_PIN_HIST;
                            string pinName = request.getPayloadAsString();
                            response.setPayloadAsEnumerable(board.getPin(pinName).History());
                            break;
                        }
                    case Commands.CONSUME_PIN_HIST:
                        {
                            response.Opcode = Commands.CONSUME_PIN_HIST;
                            string pinName = request.getPayloadAsString();
                            response.setPayloadAsEnumerable(board.getPin(pinName).ConsumeHistory());
                            break;
                        }
                    case Commands.SET_CONTROLLER:
                        {
                            response.Opcode = Commands.SET_CONTROLLER;
                            ArrayList values = request.Payload;
                            board.setOnOffController((PinValue)values[0], (PinValue)values[1], (PinValue)values[2], (PinValue)values[3]);
                            response.setPayloadAsString("ok");
                            break;
                        }
                    default:
                        throw new Exception("Unknown opcode");
                }
            }
            return response;
        }
        /*
        public Message generateSamplerMessage(int microtick)
        {
            Message response = new Message();
            response.Opcode = Commands.READ_PINS_ASYNC;
            ArrayList values = new ArrayList();
            foreach (BoardPin pin in board.getPinout())
            {
                int freq = pin.Frequency;
                if (freq <= 0) continue;
                if (pin.CanRead() && microtick % freq == 0)
                {
                    try
                    {
                        PinValue value = pin.Read();
                        values.Add(value);
                    }
                    catch (Exception ex)
                    {
                        //Cannot read this pin
                    }
                }
            }
            response.Payload = values;
            if (values.Count <= 0) return null;
            return response;
        }*/
    }
}
