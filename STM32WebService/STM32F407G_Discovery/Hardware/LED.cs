using Microsoft.SPOT.Hardware;
using System;
using System.Collections;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public static class LED
    {
        public static Hashtable pins = getPins();
        private static Hashtable getPins()
        {
            Hashtable pins = new Hashtable();
            //Stesso pin (60) di led verde, GPIO e PWM PD12
            pins.Add("PD12", (Cpu.Pin)60);

            //Stesso pin (61) di led giallo, GPIO e PWM PD13
            pins.Add("PD13", (Cpu.Pin)61);

            //Stesso pin (62) di led rosso, GPIO e PWM PD14
            pins.Add("PD14", (Cpu.Pin)62);

            //Stesso pin (63) di led blue, GPIO e PWM PD15
            pins.Add("PD15", (Cpu.Pin)63);

            return pins;
        }
        public static Cpu.Pin get(string key)
        {
            if (pins.Contains(key))
            {
                return (Cpu.Pin)pins[key];
            }
            throw new Exception("Unsupported LED pin");
        }
        public static bool contains(string key)
        {
            return pins.Contains(key);
        }
    }
}
