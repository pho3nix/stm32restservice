using System;
using System.Collections;
using Microsoft.SPOT.Hardware;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public static class ADC
    {
        public static Hashtable pins = getPins();
        private static Hashtable getPins()
        {
            Hashtable pins = new Hashtable();
            pins.Add("PA0", Cpu.AnalogChannel.ANALOG_0);
            pins.Add("PA1", Cpu.AnalogChannel.ANALOG_1);
            pins.Add("PA2", Cpu.AnalogChannel.ANALOG_2);
            pins.Add("PA3", Cpu.AnalogChannel.ANALOG_3);
            return pins;
        }
        public static Cpu.AnalogChannel get(string key)
        {
            if (pins.Contains(key))
            {
                return (Cpu.AnalogChannel)pins[key];
            }
            throw new Exception("Unsupported Analog pin");
        }
        public static bool contains(string key)
        {
            return pins.Contains(key);
        }
    }
}
