using System;
using System.Collections;
using Microsoft.SPOT.Hardware;

namespace STM32F407G_Discovery.Netmf.Hardware
{
    public static class GPIO
    {
        public static Hashtable pins = getPins();
        private static Hashtable getPins()
        {
            Hashtable pins = new Hashtable();
            pins.Add("PA0", (Cpu.Pin)0); //User button
            pins.Add("PA1", (Cpu.Pin)1);
            pins.Add("PA2", (Cpu.Pin)2);
            pins.Add("PA3", (Cpu.Pin)3);

            pins.Add("PA4", (Cpu.Pin)4);
            pins.Add("PA5", (Cpu.Pin)5);
            pins.Add("PA6", (Cpu.Pin)6);
            pins.Add("PA7", (Cpu.Pin)7);

            pins.Add("PA8", (Cpu.Pin)8);
            //pins.Add("PA9", (Cpu.Pin)9); //USB led green
            //pins.Add("PA10", (Cpu.Pin)10); //Dedicato a USB (non funziona con microusb collegato)
            //pins.Add("PA11", (Cpu.Pin)11); //Dedicato a USB (non funziona con microusb collegato)
            //pins.Add("PA12", (Cpu.Pin)12); //Dedicato a USB (non funziona con microusb collegato)

            pins.Add("PA13", (Cpu.Pin)13);
            pins.Add("PA14", (Cpu.Pin)14);
            pins.Add("PA15", (Cpu.Pin)15);

            pins.Add("PB0", (Cpu.Pin)16);
            pins.Add("PB1", (Cpu.Pin)17);
            pins.Add("PB2", (Cpu.Pin)18);
            pins.Add("PB3", (Cpu.Pin)19);

            pins.Add("PB4", (Cpu.Pin)20);
            pins.Add("PB5", (Cpu.Pin)21);
            //pins.Add("PB6", (Cpu.Pin)22); // UART TX (White)
            //pins.Add("PB7", (Cpu.Pin)23); // UART RX (Green)

            pins.Add("PB8", (Cpu.Pin)24);
            pins.Add("PB9", (Cpu.Pin)25);
            pins.Add("PB10", (Cpu.Pin)26);
            pins.Add("PB11", (Cpu.Pin)27);

            pins.Add("PB12", (Cpu.Pin)28);
            pins.Add("PB13", (Cpu.Pin)29);
            pins.Add("PB14", (Cpu.Pin)30);
            pins.Add("PB15", (Cpu.Pin)31);

            pins.Add("PC0", (Cpu.Pin)32);
            pins.Add("PC1", (Cpu.Pin)33);
            pins.Add("PC2", (Cpu.Pin)34);
            pins.Add("PC3", (Cpu.Pin)35);

            pins.Add("PC4", (Cpu.Pin)36);
            pins.Add("PC5", (Cpu.Pin)37);
            pins.Add("PC6", (Cpu.Pin)38);
            pins.Add("PC7", (Cpu.Pin)39);

            pins.Add("PC8", (Cpu.Pin)40);
            pins.Add("PC9", (Cpu.Pin)41);
            pins.Add("PC10", (Cpu.Pin)42);
            pins.Add("PC11", (Cpu.Pin)43);

            pins.Add("PC12", (Cpu.Pin)44);
            pins.Add("PC13", (Cpu.Pin)45);
            pins.Add("PC14", (Cpu.Pin)46);
            pins.Add("PC15", (Cpu.Pin)47);

            pins.Add("PD0", (Cpu.Pin)48);
            pins.Add("PD1", (Cpu.Pin)49);
            pins.Add("PD2", (Cpu.Pin)50);
            pins.Add("PD3", (Cpu.Pin)51);

            pins.Add("PD4", (Cpu.Pin)52);
            //pins.Add("PD5", (Cpu.Pin)53); //USB led red
            pins.Add("PD6", (Cpu.Pin)54);
            pins.Add("PD7", (Cpu.Pin)55);

            pins.Add("PD8", (Cpu.Pin)56);
            pins.Add("PD9", (Cpu.Pin)57);
            pins.Add("PD10", (Cpu.Pin)58);
            pins.Add("PD11", (Cpu.Pin)59);

            pins.Add("PD12", (Cpu.Pin)60); //LED Green
            pins.Add("PD13", (Cpu.Pin)61); //LED Yellow
            pins.Add("PD14", (Cpu.Pin)62); //LED Red
            pins.Add("PD15", (Cpu.Pin)63); //LED Blue

            pins.Add("PE0", (Cpu.Pin)64);
            pins.Add("PE1", (Cpu.Pin)65);
            pins.Add("PE2", (Cpu.Pin)66);
            pins.Add("PE3", (Cpu.Pin)67);

            pins.Add("PE4", (Cpu.Pin)68);
            pins.Add("PE5", (Cpu.Pin)69);
            pins.Add("PE6", (Cpu.Pin)70);
            pins.Add("PE7", (Cpu.Pin)71);

            pins.Add("PE8", (Cpu.Pin)72);
            pins.Add("PE9", (Cpu.Pin)73);
            pins.Add("PE10", (Cpu.Pin)74);
            pins.Add("PE11", (Cpu.Pin)75);

            pins.Add("PE12", (Cpu.Pin)76);
            pins.Add("PE13", (Cpu.Pin)77);
            pins.Add("PE14", (Cpu.Pin)78);
            pins.Add("PE15", (Cpu.Pin)79);
            return pins;
        }
        public static Cpu.Pin get(string key)
        {
            if (pins.Contains(key))
            {
                return (Cpu.Pin)pins[key];
            }
            throw new Exception("Unsupported GPIO pin");
        }
        public static bool contains(string key)
        {
            return pins.Contains(key);
        }
    }
}
