using Microsoft.SPOT;
using System.Threading;
using STM32F407G_Discovery.Netmf.Models;
using STM32F407G_Discovery.Netmf.Handlers;
using STM32Commons.Models;
using STM32Commons.Mapper;
using System;
using STM32Commons.Utils;
using System.Globalization;
using System.Reflection;

namespace STM32F407G_Discovery
{
    public class Program
    {
        private static Board board = null;
        private static MessageHandler handler = null;
        
        private static Thread sync = null;
        //private static Timer async = null;
        //private static int microtickCount = 0;
        //private static int macrotickCount = 60000;
        
        public static void Main()
        {

            board = new Board();
            //microtick = 0
            //macrotick = 60000 tick
            //tick = 100 ms
            board.startSampler(0, 60000, 100);

            handler = new MessageHandler(board);
            
            //async = new Timer(new TimerCallback(asyncCycle), "", 100, 100);

            sync = new Thread(syncCycle);
            sync.Start();
        }

        private static void syncCycle()
        {
            Led green = new Led(Led.GREEN_USB);
            Led red = new Led(Led.RED_USB);
            while (true)
            {
                //Debug.GC(true);
                green.off();
                string received = board.uart.ReadLine();
                Debug.Print("Received: " + received);
                red.off();

                Message request = null;
                Message response = null;

                try
                {
                    green.on();
                    red.off();
                    request = MessageMapper.deserialize(received);
                    response = handler.handleMessage(request);
                }
                catch (Exception ex)
                {

                    red.on();
                    green.off();
                    response = new Message();
                    response.Opcode = Commands.ERROR;
                    response.setPayloadAsString(ex.Message);
                }

                string tosend = MessageMapper.serialize(response);
                Debug.Print("Sending: " + tosend);
                board.uart.WriteLine(tosend);
                Thread.Sleep(100);
            }
        }

        /*
        private static void asyncCycle(object data)
        {
            microtickCount = microtickCount % macrotickCount;
            microtickCount++;
            Message response = handler.generateAsyncMessage(microtickCount);
            if (response != null)
            {
                string tosend = MessageMapper.serialize(response);
                Debug.Print("Sending Async: " + tosend);
                board.uart.WriteLine(tosend);
            }
        }*/
     }
}
