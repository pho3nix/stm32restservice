using System;
using System.Collections;

namespace STM32Commons.Models
{
    class Message
    {
        public Message() { }

        public Message(string opcode, ArrayList payload)
        {
            Opcode = opcode;
            Payload = payload;
        }

        public int Lenght { get; private set; }
        public string Opcode { get; set; }
        private ArrayList payload;
        public ArrayList Payload { get
            {
                return payload;
            }
            set
            {
                payload = value;
                if (payload != null) {
                    Lenght = payload.Count;
                }
            }
        }

        public void setPayloadAsEnumerable(IEnumerable payload)
        {
            ArrayList list = new ArrayList(); 
            foreach (object value in payload)
            {
                list.Add(value);
            }
            Payload = list;
        }

        public void setPayloadAsObject(object payload)
        {
            ArrayList list = new ArrayList();
            list.Add(payload);
            Payload = list;
        }

        public object getPayloadAsObject()
        {
            if (Payload != null && Payload.Count > 0)
            {
                return Payload[0];
            }
            return null;
        }

        public void setPayloadAsString(string payload)
        {
            ArrayList list = new ArrayList();
            list.Add(payload);
            Payload = list;
        }
        public string getPayloadAsString()
        {
            if(Payload!=null && Payload.Count > 0)
            {
                return Payload[0] as string;
            }
            return null;
        }
    }
}
