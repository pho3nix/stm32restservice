﻿namespace STM32Commons.Models
{
    public class PinValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Timestamp { get; set; }
    }
}
