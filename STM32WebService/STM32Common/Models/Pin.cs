﻿using System;

namespace STM32Commons.Models
{
    public class Pin
    {
        public Pin()
        {
            Name = null;
            SupportedModes = null;
            Mode = -1;
            Frequency = 0;
        }

        public Pin(string name) : this()
        {
            Name = name;
        }

        public Pin(string name, int[] supportedModes) : this(name)
        {
            SupportedModes = supportedModes;
        }

        public string Name { get; set; }
        public bool Enabled { get; protected set; }
        public int[] SupportedModes { get; protected set; }

        public int Frequency { get; set; }

        private int mode;
        public int Mode
        {
            get
            {
                return mode;
            }
            set
            {
                if (mode == value) return;
                disable(); //Set Enabled = false
                mode = -1;
                if (value == -1)
                {
                    return;
                }
                if (SupportedModes != null)
                {
                    foreach (int m in SupportedModes)
                    {
                        if (m == value) mode = value;
                    }
                }else
                {
                    mode = value;
                }
                if (mode == -1) throw new Exception("Unsupported pin mode");
                enable(); //Set Enabled = true
            }
        }

        protected virtual void enable()
        {
            Enabled = true;
        }

        protected virtual void disable()
        {
            Enabled = false;
        }
    }
}
