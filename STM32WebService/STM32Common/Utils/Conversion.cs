﻿using System;
using System.Text;

namespace STM32Common.Utils
{
    class Conversion
    {
        //TO REMOVE: gestione della conversione double/string con punto/virgola
        public static double tryParse(string value)
        {
            double val = 0;
            try
            {
                val = double.Parse(value);
            }
            catch (Exception e1)
            {
                StringBuilder sb1 = new StringBuilder(value);
                sb1.Replace(",", ".");
                try
                {
                    val = double.Parse(sb1.ToString());
                }
                catch (Exception e2)
                {
                    StringBuilder sb2 = new StringBuilder(value);
                    sb2.Replace(".", ",");
                    try
                    {
                        val = double.Parse(sb2.ToString());
                    }
                    catch (Exception e3)
                    {
                        throw new Exception("Invalid double value.");
                    }
                }
            }
            return val;
        }
    }
}
